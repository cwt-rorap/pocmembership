﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MembershipPostGree.Models;
using System.Configuration;
using Npgsql;
using System.Data;
using NpgsqlTypes;

namespace MembershipPostGree.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel objLogin)
        {
            

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(objLogin);
                }

                string connstring = ConfigurationManager.AppSettings["ConnectionString"].ToString();

                using (NpgsqlConnection conn = new NpgsqlConnection(connstring))
                {
                    conn.Open();
                    NpgsqlCommand comm = new NpgsqlCommand("cu_user_validate", conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    comm.Parameters.Add(new NpgsqlParameter("user_name", NpgsqlDbType.Varchar, 512));
                    comm.Parameters["user_name"].Value = objLogin.Email;

                    comm.Parameters.Add(new NpgsqlParameter("pass_word", NpgsqlDbType.Varchar, 128));
                    comm.Parameters["pass_word"].Value = objLogin.Password;

                    Int32 count = (Int32)comm.ExecuteScalar();

                    if (count == 1)
                    {
                        return RedirectToAction("DashBoard", "Home");
                        //ViewBag.ResultMessage = "Logged Successfully";
                    }
                    else
                    {
                        ViewBag.ResultMessage = "Please check username and password";
                    }
                }
            }
            catch (Exception msg)
            {
                throw msg;
            }
            
            return View(objLogin);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel objModel) 
        {
            try
            {
                string connstring = ConfigurationManager.AppSettings["ConnectionString"].ToString();

                using (NpgsqlConnection conn = new NpgsqlConnection(connstring))
                {
                    conn.Open();
                    NpgsqlCommand comm = new NpgsqlCommand("cu_insert_new_user", conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    comm.Parameters.Add(new NpgsqlParameter("user_id", NpgsqlDbType.Varchar, 100));
                    comm.Parameters["user_id"].Value = Guid.NewGuid();

                    comm.Parameters.Add(new NpgsqlParameter("user_name", NpgsqlDbType.Varchar, 512));
                    comm.Parameters["user_name"].Value = objModel.Username;

                    comm.Parameters.Add(new NpgsqlParameter("lowered_username", NpgsqlDbType.Varchar, 512));
                    comm.Parameters["lowered_username"].Value = objModel.Username.ToLower();

                    comm.Parameters.Add(new NpgsqlParameter("password", NpgsqlDbType.Varchar, 128));
                    comm.Parameters["password"].Value = objModel.Password;

                    comm.Parameters.Add(new NpgsqlParameter("is_approved", NpgsqlDbType.Bit));
                    comm.Parameters["is_approved"].Value = 1;

                    comm.Parameters.Add(new NpgsqlParameter("is_locked_out", NpgsqlDbType.Bit));
                    comm.Parameters["is_locked_out"].Value = 0;

                    Int32 count = (Int32)comm.ExecuteScalar();

                    if (count == 1)
                    {
                        ViewBag.ResultMessage = "User created successfully.";
                    }
                    else
                    {
                        ViewBag.ResultMessage = "User already exists.";
                    }
                }
            }
            catch (Exception msg)
            {
                throw msg;
            }

            return View(new RegisterModel());
        }

        //public ActionResult Roles()
        //{
        //    return View();
        //}
    }
}